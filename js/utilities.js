/**
 * @file
 * Created by sathukorala on 10/5/15.
 */

/* global Pace: true */

(function ($) {
    "use strict";
    $(document).ready(function () {
                $('#module-list').DataTable();
                    $('#module-list_filter input').addClass('form-text');
        $('#module-list_length select').addClass('form-select');
        Pace.start();
      });

    $(document).on("click", "button[id^='/project/']", function (event) {
        var str = jQuery(this).attr("id");
        var nameSplit = str.split("/");
        var moduleId = nameSplit[2];
        var sendData = {moduleId: moduleId};
        Pace.track(function () {
            $.ajax({
              url: "/module/installer",
              type: "POST",
              data: sendData,
              success: function (data) {
                    $("#content").prepend('<div id="console" class="clearfix"> <div class="messages status"> <h2 class="element-invisible">Status message</h2>' +
                    '<pre>' + data + '</pre></div></div>');
                    setTimeout(function () {
                          $('#console').fadeOut(1000);
                        }, 4000);
                  }
            });
          });
      });
  })(jQuery);
