<?php
/**
 * @file
 * Template file to render data into data tables.
 */
$main_site = "http://drupal.org";
?>
<div id="alert"> </div>
<table id="module-list" class="display" cellspacing="0" width="100%">
  <thead>
  <tr>
    <th>Module Name</th>
    <th>Module Page</th>
    <th>Install</th>
  </tr>
  </thead>
  <tbody>
    <?php
    if(!empty($page)) {
        foreach ($page as $key) {
            echo "<tr>
              <td>" . $key['index_value'] . "</td>
              <td><a target='_blank' href=" . $main_site . $key['index_key'] . " >" . $main_site . $key['index_key'] . "</a> </td>
              <td class='dt-body-center'><button type='button' id=" . $key['index_key'] . " class='btn btn-primary btn-xs submit-button'>Install</button></td>
            </tr>";
        }
    }
    ?>
  </tbody>
</table>
