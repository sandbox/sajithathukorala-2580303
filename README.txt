INTRODUCTION
------------
Easy module installer module provide functionality to install modules by
selecting all contributed module list, which is listed by crawling
drupal.org. Once user search and click on install button it will run
drush command from background to download the module. There are more than
thousand modules for drupal 7, for now we are just caching them after
first load of the module page.


REQUIREMENTS
------------
This module requires the followings:
 * Should install drush on your server

This module requires the following modules:
 * jQuery Update (https://www.drupal.org/project/jquery_update)


INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Download the latest DataTables jQuery plugin 1.10.9 package from:
       http://datatables.net/download/packages

   * Extract the archive(DataTables-1.10.9.zip) and move the
     DataTables-1.10.9/media folder to the sub-directory called data_tables
     in the easy_module_installer/packages folder:

       sites/all/modules/easy_module_installer/packages/data_tables

     The final path to the media folder should be:

       sites/all/modules/easy_module_installer/packages/data_tables/media

 * Download the latest Pace js jQuery plugin from :
       http://github.hubspot.com/pace/docs/welcome/
       (https://raw.github.com/HubSpot/pace/v1.0.0/pace.min.js)

   * And move the pace.min.js file to the sub-directory called pace_js
     in the easy_module_installer/packages folder:

       sites/all/modules/easy_module_installer/packages/pace_js

     The final path to the pace.min.js file should be:

       sites/all/modules/easy_module_installer/packages/pace_js/pace.min.js



CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Easy Module Installer » Install contribute modules

     Users in roles with the "Install contribute modules" permission
     will see the admin/modules/easy-installer page.

TROUBLESHOOTING
---------------

 * If the menu does not display, check the following:

   - Check the "Install contribute modules (access_easy_module_installer)"
     permissions enabled for the appropriate roles?

 * If module list page is empty

   - Check your internet connection (If your in local server).

 * If module does not install properly, check the following:

   - Check whether your server has installed Drush properly.
   - Check whether you have permission to run the bash scripts.

MAINTAINERS
-----------

Current maintainers:
 * Sajith Athukorala (SajithAthukorala) - https://www.drupal.org/user/2231444

This project has been sponsored by:
 * Zaizi Asia (Pvt) Ltd
   Award Winning Integrators Of Open Source Technology
   We Develop, Implement And Support Smart Content Solutions So That Your Team
   Works Together More Effectively.
   Visit https://www.zaizi.com for more information.
