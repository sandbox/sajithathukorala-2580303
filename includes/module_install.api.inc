<?php

/**
 * @file
 * This file contains shell and drush commands.
 */

/**
 * Menu call back function to Install Module.
 */
function easy_module_installer() {
  $module_id = $_POST['moduleId'];
  $module_exists = drupal_get_path('module', $module_id);
  if (module_exists($module_id)) {
    echo $module_id . ' module is already exist and enabled';
  }
  elseif (!empty($module_exists)) {
    echo $module_id . " module is already exist";
  }
  else {
    $module_list = "drush -y dl " . $module_id;
    exec(($module_list), $rttr, $str);
    echo $module_id . " module installed successfully";
  }
}
