<?php

/**
 * @file
 * Use to get the module details by crawling drupal org.
 */

/**
 * Menu callback to load the theme.
 */
function easy_module_installer_module_list() {
  drupal_add_js(drupal_get_path('module', 'easy_module_installer') . '/packages/data_tables/media/js/jquery.dataTables.min.js');
  drupal_add_js(drupal_get_path('module', 'easy_module_installer') . '/packages/pace_js/pace.min.js');
  drupal_add_js(drupal_get_path('module', 'easy_module_installer') . '/js/utilities.js');
  drupal_add_css(drupal_get_path('module', 'easy_module_installer') . '/packages/data_tables/media/css/jquery.dataTables.min.css');
  drupal_add_css(drupal_get_path('module', 'easy_module_installer') . '/css/utilities.css');
  drupal_add_css(drupal_get_path('module', 'easy_module_installer') . '/css/pace-theme-loading-bar.css');

  /* a link to search */
  $link = "https://www.drupal.org/project/project_module/index?project-status=full&drupal_core=103";

  $cache_id = "easy_module_installer_caching";
  $urls = cache_get($cache_id, 'cache');

  if (empty($urls)) {
    /* get the links */
    $urls = easy_module_installer_get_links($link);
    cache_set($cache_id, $urls, 'cache', CACHE_PERMANENT);
  }
  else {
    $urls = $urls->data;
  }

  /* check for results */
  if (count($urls) > 0) {
    foreach ($urls as $key => $value) {
      if (substr($key, 0, 9) == '/project/') {
        $page[] = array(
          'index_key' => $key,
          'index_value' => $value,
        );
      }
    }
  }
  else {
    echo "No links found at $link";
  }
  return theme('module_installer_module_list_theme', array('page' => $page));
}

/**
 * Used to get the module list links from dom.
 *
 * @param string $link
 *   Drupal org path to crawl.
 *
 * @return array
 *   Set of links.
 */
function easy_module_installer_get_links($link) {

  $ret = array();

  /* a new dom object */
  $dom = new domDocument();

  /* get the HTML (suppress errors) */
  @$dom->loadHTML(file_get_contents($link));

  /* remove silly white space */
  $dom->preserveWhiteSpace = FALSE;

  /* get the links from the HTML */
  $links = $dom->getElementsByTagName('a');

  /* loop over the links */
  foreach ($links as $tag) {
    $ret[$tag->getAttribute('href')] = $tag->childNodes->item(0)->nodeValue;
  }
  return $ret;
}
